# **Trabajo Practico de Semáforos**
### SISTEMAS OPERATIVOS 
Lic.en Sistemas de Información 

Universidad Nacional de Luján

Año: 2016

Estudiante: Cuestas Yamila - Legajo: 108897

## **Objetivo del Trabajo**

El presente trabajo tiene como objetivo probar el código de semáforos presentado por el docente de la asignatura. Se intentará lograr corregir los errores, probar el programa y comprobar el correcto funcionamiento. 

Se comienza con una lectura del código fuente presentado para una primera aproximación y se procede luego a la compilación del mismo. En caso de encontrar fallas, se corregirán los errores y se volverá a reintentar hasta lograr el la correcta compilación y obtener el ejecutable `usem` que permita avanzar sobre el trabajo. 

Una vez compilado se realizaran las pruebas necesarias para comprobar el correcto funcionamiento del mismo. Vale decir, que la lógica del programa coincida con lo que esperamos que suceda por parte de una implementanción de semáforos. 


### Características del entorno de ejecución: 
* Sistema Operativo: Ubuntu 14.04 LTS – 64-bit
* Kernel: Linux 3.13.0-88-generic x86_64
* Versión GCC: gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
* Procesador: Intel® Core™ i5-3317U CPU @ 1.70GHz × 4
* Memoria: 6 Gb.


##** ETAPA 1**
## **Compilación**

A continuación intentamos compilar el código provisto en el tp de semáforos y comprobar los errores que puedan surgir. 



### 1.Primer Error:

    
```
#!bash

$ gcc usem.c -o usem
    usem.c: In function ‘crear_sem’:
    usem.c:76:14: error: storage size of ‘semopciones’ isn’t known
      union semun semopciones;
                  ^
    usem.c:77:16: error: ‘SEMMSL’ undeclared (first use in this           function)
      if(cantidad > SEMMSL) {
                    ^
    usem.c:77:16: note: each undeclared identifier is reported only once for each function it appears in
    usem.c: In function ‘zero_espera_sem’:
    usem.c:110:45: error: ‘errno’ undeclared (first use in this function)
       fprintf(stderr, "Valor de ERRNO : %d \n", errno);
                                                 ^

```


__Solución:__ Incluimos la librería:

  
```
#!c

  #include <linux/sem.h>
```




### 2.Segundo Error:

    $ gcc usem.c -o usem
    In file included from /usr/include/linux/sem.h:4:0,
                     from usem.c:9:
    /usr/include/linux/ipc.h:9:8: error: redefinition of ‘struct ipc_perm’
     struct ipc_perm
            ^
    In file included from /usr/include/x86_64-linux-gnu/sys/ipc.h:29:0,
                     from usem.c:7:
    /usr/include/x86_64-linux-gnu/bits/ipc.h:42:8: note: originally defined here
     struct ipc_perm
            ^
    In file included from usem.c:9:0:
    /usr/include/linux/sem.h:23:8: error: redefinition of ‘struct semid_ds’
     struct semid_ds {
            ^
    In file included from /usr/include/x86_64-linux-gnu/sys/sem.h:30:0,
                     from usem.c:8:
    /usr/include/x86_64-linux-gnu/bits/sem.h:38:8: note: originally defined here
     struct semid_ds
            ^
    In file included from usem.c:9:0:
    /usr/include/linux/sem.h:38:8: error: redefinition of ‘struct sembuf’
     struct sembuf {
            ^
    In file included from usem.c:8:0:
    /usr/include/x86_64-linux-gnu/sys/sem.h:41:8: note: originally defined here
     struct sembuf
            ^
    In file included from usem.c:9:0:
    /usr/include/linux/sem.h:53:9: error: redefinition of ‘struct seminfo’
     struct  seminfo {
             ^
    In file included from /usr/include/x86_64-linux-gnu/sys/sem.h:30:0,
                     from usem.c:8:
    /usr/include/x86_64-linux-gnu/bits/sem.h:72:9: note: originally defined here
     struct  seminfo
             ^
    usem.c: In function ‘bloquear_sem’:
    usem.c:98:2: warning: passing argument 2 of ‘semop’ from incompatible pointer type [enabled by default]
      if((semop(sid, &sembloqueo, 1)) == -1) {
      ^
    In file included from usem.c:8:0:
    /usr/include/x86_64-linux-gnu/sys/sem.h:58:12: note: expected ‘struct sembuf *’ but argument is of type ‘struct sembuf *’
     extern int semop (int __semid, struct sembuf *__sops, size_t __nsops) __THROW;
                ^
    usem.c: In function ‘zero_espera_sem’:
    usem.c:109:2: warning: passing argument 2 of ‘semop’ from incompatible pointer type [enabled by default]
      if((semop(sid, &esperazero, 1)) == -1) {
      ^
    In file included from usem.c:8:0:
    /usr/include/x86_64-linux-gnu/sys/sem.h:58:12: note: expected ‘struct sembuf *’ but argument is of type ‘struct sembuf *’
     extern int semop (int __semid, struct sembuf *__sops, size_t __nsops) __THROW;
                ^
    usem.c:111:45: error: ‘errno’ undeclared (first use in this function)
       fprintf(stderr, "Valor de ERRNO : %d \n", errno);
                                                 ^
    usem.c:111:45: note: each undeclared identifier is reported only once for each function it appears in
    usem.c: In function ‘desbloquear_sem’:
    usem.c:122:2: warning: passing argument 2 of ‘semop’ from incompatible pointer type [enabled by default]
      if((semop(sid, &semdesbloqueo, 1)) == -1) {
      ^
    In file included from usem.c:8:0:
    /usr/include/x86_64-linux-gnu/sys/sem.h:58:12: note: expected ‘struct sembuf *’ but argument is of type ‘struct sembuf *’
     extern int semop (int __semid, struct sembuf *__sops, size_t __nsops) __THROW;
        ^

__Solución:__ Borrar la linea:

    #include <sys/sem.h>




### 3.Tercer Error:

Luego, volvemos a intentar compilar y nos tira el siguiente error:

    $ gcc usem.c -o usem
    usem.c: In function ‘zero_espera_sem’:
    usem.c:107:45: error: ‘errno’ undeclared (first use in this function)
       fprintf(stderr, "Valor de ERRNO : %d \n", errno);
                                                 ^
    usem.c:107:45: note: each undeclared identifier is reported only once for each function it appears in


__Solución:__ Agrega la libreria  <errno.h>

```
#!c


    +#include <errno.h>
```

        
        
### 4. Compilamos nuevamente:
Compilamos nuevamente:

    $ gcc usem.c -o usem

En esta oportunidad la compilación fue exitosa!!!
        
##** ETAPA 2**
##**Comprobación del Funcionamiento** 

### Creación y borrado de Semáforos 

Creamos un semáforo:        

    $ ./usem c
    Creando nuevo conjunto IPC con 1 semáforo(s)...
    Nuevo conjunto IPC de sem. creado con éxito


Luego volvemos a intentar volver a crear el semáforo y como ya existe, nos devuelve un mensaje
de que ya existe un conjunto con esta clave, lo cual es correcto.

    $ ./usem c
    Creando nuevo conjunto IPC con 1 semáforo(s)...
    Ya existe un conjunto con esta clave!

Borramos el semáforo y luego volvemos a ejecutar la creación sin errores.


### Función Agregar:

    $ ./usem a test "probando la funcion agregar"
    $ cat test 
    probando la funcion agregar

Funciona Correctamente

### Función Tomar recurso

Desde una terminal tomamos el recurso:

    $ ./usem t

Luego, abrimos otra terminal e intentamos escribir sobre el archivo:

    $ ./usem a test 'Esto es un mensaje desde otra terminal'

El archivo 'test' sigue conteniendo la linea generada en la prueba anterior:

    $ cat test 
    probando la funcion agregar

Vemos además, que el archivo no fue escrito porque esta bloqueado por el semáforo. Se queda por lo tanto esperando que este sea desbloqueado.


Luego, Desbloqueamos el semáforo y se escribe en el archivo la linea correspondiente que estaba bloqueada esperando la liberación del recurso.

    $ ./usem t
    
    $ 


Contenido del archivo 'test':

    $ cat test 
    probando la funcion agregar
    Esto es un mensaje desde otra terminal


### Comprobamos el correcto funcionamiento de la opción E

ejecutamos: 

    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem e
    Proceso a la ESPERA de valor CERO en semáforo IPC...
    La Espera NO pudo establecerse 
    Valor de ERRNO : 22 

Verificando el código fuente, comprobamos que falta el llamado a  abrir_sem, por lo cual el proceso no puede establecer la correcta relación con el semáforo.

Corregimos el codigo agregando la linea correspondiente:

        
```
#!c

switch(tolower(argv[1][0])) {
            // Crear semaforo
            case 'c': if(argc != 2) 
                    uso();
                    crear_sem(&semset_id, clave, 1);
                    break;
            // Bloquear semaforo
            case 't': 
                    abrir_sem(&semset_id, clave);
                    bloquear_sem(semset_id, INI);
                    getchar();
                    desbloquear_sem(semset_id, INI);
                    break;
            // Esperar a que el valor del semaforo sea 0
            case 'e':
                    abrir_sem(&semset_id, clave); // Linea Agregada
                    zero_espera_sem(semset_id, INI);
                    break;
            case 'b': 
                    abrir_sem(&semset_id, clave);
                    remover_sem(semset_id);
                    break;
            case 'a': abrir_sem(&semset_id, clave);
                    bloquear_sem (semset_id, INI);
                    if ((fptr = fopen(argv[2], "a")) == NULL) exit (-1);
                    else {
                        fprintf(fptr, "%s\n", argv[3]);
                        fclose(fptr);
                    }
                    desbloquear_sem (semset_id, INI);
                    break;
            default: uso();
        }
        return(0);
    } 
```


Volvemos a compilar:

    $ gcc usem.c -o usem


Ejecutamos la opción __e__ nuevamente:

    $./usem e
    Proceso a la ESPERA de valor CERO en semáforo IPC...

En otra terminal ejecutamos 

    $ ./usem a test 'comprobando el parametro E'

Dando como resultado que en la termina 1 muestre lo siguiente:

    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem e
    Proceso a la ESPERA de valor CERO en semáforo IPC...
    ESPERA concluída. Terminación del proceso.


### Otras comprobaciones:

Comprobamos el correcto manejo de errores cuando el semáforo no existe:

    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem e
    El conjunto de semáforos NO existe!
    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem t
    El conjunto de semáforos NO existe!
    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem a test2 'linea'
    El conjunto de semáforos NO existe!
    yamila@yamila-np530:~/Unlu/Sistemas Operativos/git/sistemas-operativos/1_TP_A_Semaforos$ ./usem a test2 'linea'
    El conjunto de semáforos NO existe!

Todas las comprobaciones funcionan correctamente. 


## **Conclusiones:**

Para que el código compile correctamente fue necesario incluir las librerías ´<linux/sem.h>´ y ´<errno.h>´. Ademas se debió borrar la librería ´<sys/sem.h>´. 


Una vez que se logro compilar el programa ´usem.c´, el error encontrado fue cuando se utilizaba la opción e que espera a que el valor del semáforo seleccionado sea cero. Se solucionó agregando dentro de las funciones que son llamadas la de ´abrir_sem(&semset_id, clave)´. 

Luego de volver a compilar el semáforo funciono correctamente.


###** Código final**


```
#!c

/****************************************************************************/
/* - usem.c - Utilitario básico para semáforos IPC System V -*/
/****************************************************************************/
// Asignatura: Sitemas Operativos - 2016
// Univerisdad Nacional de Lujan
// Cuestas Yamila

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <linux/ipc.h>
// #include <sys/sem.h> // Borrada por error 2
#include <linux/sem.h> // Agregado por error 1
#include <errno.h> // Agregada por error 3

#define SEM_MAX_RECURSO   1     /* Valor inicial de todos los semáforos */
#define INI	              0     /* Índice del primer semáforo */

void abrir_sem(int *sid, key_t clave);
void crear_sem(int *sid, key_t clave, int idx);
void bloquear_sem(int clave, int idx);
void zero_espera_sem(int clave, int idx);
void desbloquear_sem(int clave, int idx);
void remover_sem(int sid);
void uso(void);

int main(int argc, char *argv[]) {
	key_t clave;
	int	semset_id;
	FILE *fptr;

	if (argc == 1)
		uso();

	/* Crea una clave única mediante ftok() */
	clave = ftok(".", 'u');

	switch(tolower(argv[1][0])) {
		// Crear semaforo
		case 'c': if(argc != 2) 
				uso();
				crear_sem(&semset_id, clave, 1);
				break;
		// Bloquear semaforo
		case 't': 
				abrir_sem(&semset_id, clave);
				bloquear_sem(semset_id, INI);
				getchar();
				desbloquear_sem(semset_id, INI);
				break;
		// Esperar a que el valor del semaforo sea 0
		case 'e':
				abrir_sem(&semset_id, clave); // Linea Agregada
				zero_espera_sem(semset_id, INI);
				break;
		case 'b': 
				abrir_sem(&semset_id, clave);
				remover_sem(semset_id);
				break;
		case 'a': abrir_sem(&semset_id, clave);
				bloquear_sem (semset_id, INI);
				if ((fptr = fopen(argv[2], "a")) == NULL) exit (-1);
				else {
					fprintf(fptr, "%s\n", argv[3]);
					fclose(fptr);
				}
				desbloquear_sem (semset_id, INI);
				break;
		default: uso();
	}
	return(0);
}

void abrir_sem(int *sid, key_t clave) {
	/* Abre el conjunto de semáforos */
	if((*sid = semget(clave, 0, 0666)) == -1)
		{
			printf("El conjunto de semáforos NO existe!\n");
			exit(1);
		}
}


void crear_sem(int *sid, key_t clave, int cantidad) {
	int cntr;
	union semun semopciones;
	if(cantidad > SEMMSL) {
		printf("ERROR : cant. máx. de sem. en el conjunto es %d\n", SEMMSL);
		exit(1);
	}
	printf("Creando nuevo conjunto IPC con %d semáforo(s)...\n",cantidad);
	if((*sid = semget(clave, cantidad, IPC_CREAT|IPC_EXCL|0666)) == -1) {
		fprintf(stderr, "Ya existe un conjunto con esta clave!\n");
		exit(1);
	}
	printf("Nuevo conjunto IPC de sem. creado con éxito\n");
	semopciones.val = SEM_MAX_RECURSO;
	/* Inicializa todos los semáforos del conjunto */
	for(cntr=0; cntr<cantidad; cntr++)
	semctl(*sid, cntr, SETVAL, semopciones);
}


void bloquear_sem(int sid, int idx) {
	struct sembuf sembloqueo={ 0, -1, SEM_UNDO};
	sembloqueo.sem_num = idx;
	if((semop(sid, &sembloqueo, 1)) == -1) {
		fprintf(stderr, "El Bloqueo falló\n");
		exit(1);
	}
}

void zero_espera_sem(int sid, int idx) {
	struct sembuf esperazero = { 0, 0, SEM_UNDO};
	esperazero.sem_num = idx;
	
	printf("Proceso a la ESPERA de valor CERO en semáforo IPC...\n");
	
	if((semop(sid, &esperazero, 1)) == -1) {
		fprintf(stderr, "La Espera NO pudo establecerse \n");
		fprintf(stderr, "Valor de ERRNO : %d \n", errno);
		exit(1);
	}
	printf("ESPERA concluída. Terminación del proceso.\n");
}


void desbloquear_sem(int sid, int idx) {
	struct sembuf semdesbloqueo={ 0, 1, SEM_UNDO};
	semdesbloqueo.sem_num = idx;
	/* Intento de desbloquear el semáforo */
	if((semop(sid, &semdesbloqueo, 1)) == -1) {
		fprintf(stderr, "El Desbloqueo falló\n");
		exit(1);
	}
}

void remover_sem(int sid) {
	semctl(sid, 0, IPC_RMID, 0);
	printf("Conjunto de semáforos eliminado\n");
}


void uso(void) { 
	fprintf(stderr, " - usem - Utilitario básico para semáforos IPC  \n"); 
	fprintf(stderr, "    USO : usem   (c)rear                       \n"); 
	fprintf(stderr, "                  (t)oma recurso compartido     \n");
	fprintf(stderr, "                  (e)spera IPC de valor cero    \n");
	fprintf(stderr, "                  (b)orrar                      \n"); 
	fprintf(stderr, "                  (a)gregar <PATH-DB> <LINE>    \n"); 
	exit(1);
}

/****************************************************************************/
/**************************** fín de usem.c *********************************/
/****************************************************************************/
```