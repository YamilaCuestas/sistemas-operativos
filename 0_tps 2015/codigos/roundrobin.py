#!/usr/bin/env python
#-*- coding:utf-8 -*-
# 

terminado = 1
noTerminado = 0

class Proceso():
    
    def __init__(self, nombre, tiempo):
        self.nombre = nombre
        self.tiempo=tiempo
        self.estado=noTerminado
    
    def ejecutar(self, q):
        self.tiempo -= q
        if self.tiempo == 0:
            self.estado = terminado
    
    def __str__(self):
        return "%s (%d)" % (self.nombre, self.tiempo)
        

def rr(procesos=[], q=1, ):
    print "proceso / tiempo"
    for p in procesos:
        print p

    tiempo = 0 # tiempo total transcurrido

    while len(procesos)>0:
        for proc in procesos:
            t = proc.tiempo
            if t>q: # si el proceso no termina...
                print tiempo, ": ejecutando proceso", proc
                proc.ejecutar(q)
                tiempo += q
            else: # si el proceso termina...
                print tiempo, ": ejecutando proceso", proc
                proc.ejecutar(t)
                tiempo += t
                print tiempo, ": terminado proceso", proc
        procesos = filter(lambda proc: proc.estado is noTerminado, procesos)
    print "Fin del Round Robin. Tiempo total:", tiempo
            
                
## creo procesos cualquiera con tiempos cualquiera
nombres = 'ABCDEFGHI'
tiempos = []
pr = []

from random import randint

for i in range(10):
    tiempos.append(randint(1,10))

for n,t in zip(nombres, tiempos):
    p = Proceso(n,t)
    pr.append(p)

rr(pr, 3)