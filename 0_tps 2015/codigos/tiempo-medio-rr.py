#!/usr/bin/env python
#-*- coding:utf-8 -*-
# -----------------------------------------------------------------------------
# TIEMPO MEDIO ROUND ROBIN
# Realice un programa que calcule el tiempo medio de espera para el método de
# administración de Round Robin. Considere un tiempo de conmutación de Q/10.
# -----------------------------------------------------------------------------
# Asignatura: Sistemas Operativos.
# Lic. En Sistemas de Informacion - Universidad nacional de luján

import random

Q = 10 # Cantidad maxima de tiempo de CPU que se le permite consumir a cada proceso ininterrumpidamente
conmutacion = Q/10 # Tiempo en cambiar de contexto

class Proceso:
	def __init__(self, nombre, tiempo_proceso):
		self.nombre = nombre
		self.tiempo_proceso = tiempo_proceso
		self.resto = self.tiempo_proceso
		self.estado = 'Listo'
		self.tiempo_de_espera = 0

	
def ejecutar(proceso):
	proceso.resto = proceso.resto - 1
	if proceso.resto == 0:
		proceso.estado = 'Finalizado' # Finalizado
	return proceso

def imprimir_proceso(p):
		#print  p.nombre+':'+ str(p.tiempo_proceso) + ' Estado:' + p.estado + ' -  Resto:' + str(p.resto) + ' -  Espera:' + str(p.tiempo_de_espera)
		print  p.nombre+':'+ str(p.tiempo_proceso)

def round_robin(listos):
	# Listado de procesos que terminaron su ejecucion
	tiempo_total = 0
	terminados = []
	while len(listos)>0: # Mientras existan procesos para ser ejecutados
		i = 0
		while i <= (len(listos)-1):	
		# for e in listos: # Voy tomando de la lista de procesos listos
			e = listos[i]
			tiempo_CPU = 0
			while (tiempo_CPU < Q) and (e.estado != 'Finalizado'):
				e = ejecutar(e)
				tiempo_CPU += 1
				tiempo_total += 1
			# Sumar los tiempos de espera a todos los procesos menos al que fue ejecuatado
			for a in listos:
				if a != e:
					a.tiempo_de_espera = a.tiempo_de_espera + tiempo_CPU
			# Verificar si el proceso termino y de ser asi pasarlo a la lista de terminados
			if e.estado == 'Finalizado':
				terminados.append(e)
				listos.remove(e)
			else:
				i += 1
	print 'Tiempo Total:' + str(tiempo_total)
	return terminados


def promedio(procesos):
	cantidad = len(procesos)
	suma = 0
	for i in procesos:
		suma = suma + i.tiempo_de_espera
	promedio = suma/ cantidad

	print 'Promedio: ' + str(promedio) + 'ms'




def Crear_procesos(cantidad,valor_min_tiempo, valor_max_tiempo):
	procesos = []
	for x in range(0,cantidad):
		nombre = 'P'+str(x+1)
		# tiempos de los procesos generados al azar
		tp = random.randint(valor_min_tiempo,valor_max_tiempo)
		# Creo el nuevo objeto proceso
		p = Proceso(nombre, tp)
		procesos.append(p)
	print 'Procesos creados correctamente: .....'
	for i in procesos:
		imprimir_proceso(i) 
	print '\n'
	return procesos



if __name__ == '__main__':
	# Listado de objetos procesos listos para ser ejecutados
	listos = Crear_procesos(10,1,50)
	print '\nRound Robin'
	terminados = round_robin(listos)
	promedio(terminados)
